---
layout: handbook-page-toc
title: MLOps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## MLOps Single-Engineer Group

DRI: [@eduardobonet](https://gitlab.com/eduardobonet)

MLOps is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

### Vision

Make GitLab the perfect companion for a Machine Learning Engineer or Data Scientist.

### Mission

Identify opportunities in our portfolio to explore ways where GitLab can provide a better user experience for Data Science and Machine Learning across the entire Machine Learning life cycle (model creation, testing, deployment, monitoring, and iteration).

### Weekly updates

[Subscribe to the issue for updates](https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16)

### What is MLOps?

[Check out the MLOps Primer](/handbook/engineering/incubation/mlops/modelops-primer.html)

___

This group will enable data teams to build, test, and deploy their machine learning models. These features will be net new functionality within GitLab and bridge the gap between DataOps teams and ML/AI within production. In addition, MLOps will provide tuning, testing, and deployment of machine learning models, including version control and partial rollout and rollback.

**Model**

Algorithm selection is based on data shape and analytics doing parameter tuning, feature selection, and data selection. The git-focused functionality includes hosting machine learning models. Examples include JupyterHub and Anaconda as well as DVC, Dolt, and Delta lake.

**Train**

Provide data teams with the tools to take raw large data sets, clean / morph / wrangle the data, and import the sanitized data into their models to prepare for deployment. Examples of this include Trifacta, TensorFlow Serving, UbiOps, and Sagemaker.

**Test**

Verify everything works as expected and is ready for deployment. Examples of this include PyTest, PyTorch, Keras, and Scikit-learn.

**Deploy**

Enable data teams to deploy their data models including partial rollout, partial rollback, and versioning of training data. Examples of this include Kubeflow and CML.

Our MLOps focus could also start with a focus on enabling partners to integrate into GitLab properly. Potential partners include Domino Data Lab, Determined AI, and Maiot.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329591](https://gitlab.com/gitlab-org/gitlab/-/issues/329591)

### Glossary

* **Experiment** : A machine learning experiment is a set of variables that vary across multiple runs to observe and learn from the effects of changes.

* **MLOps**: MLOps is the process of taking an experimental Machine Learning model into a production web system.

* **ModelOps** : ModelOps is a superset of ML/AI functionality including MLOps and DataOps. More information [here](/direction/modelops/).


### Reading List

#### Internal Discussions
* [ModelOps Discussion w/ Board Members](https://docs.google.com/document/d/1ONYRRw7tSOpGcETiM2J_P-Df58MLi3PKGRXfl5iIYL8/edit)
* [GitLab ModelOps](https://docs.google.com/document/d/1jI1z3aT8kpF20vlxKeTteowVGfFJEh3BmnzG-cjtnZU/edit?ts=60538b97)
* [MLOps with Gitlab Paper](https://docs.google.com/document/d/1uMmB8gad7fe28aZCSBOIK6z9CbCtDmFV2ZUUuHH1YcY/edit#)
* [AI/ML Customers at Gitlab](https://docs.google.com/document/d/1QuR-kdFHYbb7HiBpJ36U3znd70juFkduVXVVY3YNd5A/edit)
* [https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB](https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB)

#### Industry news
* [6 May 2021 - Introducing GitLab, Bitbucket Cloud, and Bitbucket Server source code management for Algorithmia](https://algorithmia.com/blog/introducing-gitlab-bitbucket-cloud-and-bitbucket-server-source-code-management-for-algorithmia)
* [31 Aug 2021 - DataBricks announce 1.6b Series H funding](https://sprou.tt/1lE9mOagwDU)
